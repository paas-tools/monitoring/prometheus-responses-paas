# Introduction
This project stores a set of responses to Prometheus alarms from https://gitlab.cern.ch/paas-tools/monitoring/prometheus-openshift/

# Resources

This project makes use of the service account `builder`, created by default on `paas-infra` namespace.
It is required to mirror this project's Docker image into Openshift's internal registry.

# Configuration

This configuration requires to have a `receiver-job-definitions` configMap on the Prometheus instance where each key corresponds to a different alarm response.

In order to respond to a Prometheus alert, the alert definition needs to have the following annotations:

* `firing_job`: This annotation corresponds to the key on the `receiver-job-definitions` configMap with the job response to run when the Prometheus rule fires an alert.
* `resolved_job`: This annotation corresponds to the key on the `receiver-job-definitions` configMap with the job response to run when the Prometheus rule sends the resolution to an alert.

Job definitions are stored on the `receiver-job-definitions` configMap as YAML. 

They need a generate name property indicating whether it is a firing or resolved alarm, as well as refering to the alarm name, such as in this example:

```bash
...
apiVersion: batch/v1
kind: Job
metadata:
  generateName: firing-gitlab-elasticsearch-cluster-not-healthy-response-
  labels:
    app: prometheus-webhook-receiver
spec:
  parallelism: 1
  completions: 1
  template:
      labels:
        app: prometheus-webhook-receiver
      spec:
        containers:
        - name: ansible-job
          image: gitlab-registry.cern.ch/paas-tools/monitoring/prometheus-responses:latest
          args:
          - bash
          - -c
          - |-
            ansible-playbook gitlab_elasticsearch_cluster_not_healthy.yml
          envFrom:
          - configMapRef:
              name: prometheus-webhook-job-secrets
        imagePullPolicy: Always
        restartPolicy: Never
        serviceAccount: <desired-sa>
        serviceAccountName: <desired-sa>
# OR

apiVersion: batch/v1
kind: Job
metadata:
  generateName: resolved-gitlab-elasticsearch-cluster-not-healthy-response-
  labels:
    app: prometheus-webhook-receiver
spec:
  parallelism: 1
  completions: 1
  template:
      metadata:
        generateName: resolved-gitlab-elasticsearch-cluster-not-healthy-response-
      labels:
        app: prometheus-webhook-receiver
      spec:
        containers:
        - name: ansible-job
          image: gitlab-registry.cern.ch/paas-tools/monitoring/prometheus-responses:latest
          args:
          - bash
          - -c
          - |-
            ansible-playbook gitlab_elasticsearch_cluster_healthy.yml
          envFrom:
          - configMapRef:
              name: prometheus-webhook-job-secrets
        imagePullPolicy: Always
        restartPolicy: Never
        serviceAccount: <desired-sa>
        serviceAccountName: <desired-sa>
```

